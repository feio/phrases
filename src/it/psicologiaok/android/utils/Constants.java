package it.psicologiaok.android.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public interface Constants {
	
	final String TAG = "PsicologiaOk"; 
	
	final String TIME_SEPARATOR = ":";

	final String WEBSITE = "http://www.psicologiaok.com";
	final String FACEBOOK_PROFILE = "PsicologiaOK";
	final String FACEBOOK_PROFILE_ID = "279681748751331";
//	final String GOOGLEPLUS_PROFILE = "102629628248642947234";
	final String GOOGLEPLUS_PROFILE = "107928579034720908309";
	final String YOUTUBE_PROFILE = "psicologiaokvideo";
//	final String TWITTER_PROFILE = "1346352462";
	final String TWITTER_PROFILE = "PsicologiaOK";

	final String SELECTION = "SELECTION";
	final String MESSAGE = "MESSAGE";
	final String SECTION_INDEX = "SECTION_INDEX";
	final String WEEK_SELECTION = "WEEK_SELECTION";	
	
	final int ALARM_CODE = 12345;
	final String PREFERENCES = TAG + "_PREFERENCES";
	final String myString = "myString";

	final int JOB = 0, HEALT = 1, LOVE = 2, PERSONAL = 3, SELECTION_SIZE = 4;
	final String BG_ALPHA = "EE";
	final String BG_JOB = "e85d00", BG_HEALT = "006600", BG_LOVE = "660033", BG_PERSONAL = "000066";
	
	// Nomi activity da inviare a Google Analytics
	public static final Map<String, String> GA_ACTIVITY_NAMES = Collections
			.unmodifiableMap(new HashMap<String, String>() {
				private static final long serialVersionUID = 1031432838063498009L;
				{
					put("MainActivity", "Schermata principale");
					put("AboutActivity", "Schermata About");
					put("AlarmActivity", "Schermata frase");
					put("", "Altro");
				}
			});
}
