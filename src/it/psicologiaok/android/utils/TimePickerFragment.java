package it.psicologiaok.android.utils;

import it.psicologiaok.R;
import it.psicologiaok.android.MainActivity;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;

public class TimePickerFragment extends DialogFragment {

	TextView timer_label;
	private MainActivity mActivity;
	private OnTimeSetListener mListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (MainActivity) activity;
		
		try {
			mListener = (OnTimeSetListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnTimeSetListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
//		final Calendar c = Calendar.getInstance();
//		int hour = c.get(Calendar.HOUR_OF_DAY);
//		int minute = c.get(Calendar.MINUTE);
		
		TextView timer_label = (TextView)mActivity.findViewById(R.id.timer_label);
		int hour = mActivity.stringToTime(timer_label.getText().toString())[0];
		int minute = mActivity.stringToTime(timer_label.getText().toString())[1];

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(mActivity, mListener, hour, minute, true);
	}


	// @Override
	// public void onTimeSet(TimePicker view, int hour, int minute) {
	// // ((TextView)getActivity().findViewById(R.id.timer_label)).setText("fava");
	// // getActivity().seta
	// }
}