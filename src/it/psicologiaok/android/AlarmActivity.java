package it.psicologiaok.android;

import it.psicologiaok.R;
import it.psicologiaok.android.utils.Constants;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;
import android.text.Html;

public class AlarmActivity extends FragmentActivity {

	private ImageView close;
	private TextView text, share;
	
	@SuppressLint("NewApi") @SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_alarm);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		int sectionIndex = extras.getInt(Constants.SECTION_INDEX);
		final String msgStr = extras.getString(Constants.MESSAGE);

		text = ((TextView) findViewById(R.id.alarmText));
		text.setText(Html.fromHtml(msgStr));
		text.setTextSize(18F);

		close = (ImageView) findViewById(R.id.close);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		share = (TextView) findViewById(R.id.share);
		share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				share(msgStr);
			}
		});

		// Creazione sfondo arrotondato e colorazione
		int color = selectBackground(sectionIndex);
		RoundRectShape rect = new RoundRectShape(new float[] { 30, 30, 30, 30, 30, 30, 30, 30 }, null, null);
		ShapeDrawable bg = new ShapeDrawable(rect);
		bg.getPaint().setColor(color);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			close.getRootView().setBackgroundDrawable(bg);
		} else {
			close.getRootView().setBackground(bg);
		}
	}


	/**
	 * Colore specifico per tematica
	 * 
	 * @param sectionIndex
	 * @return
	 */
	private int selectBackground(int sectionIndex) {
		int color = Color.parseColor("#" + Constants.BG_ALPHA + Constants.BG_PERSONAL);;
		if (sectionIndex == Constants.JOB)
			color = Color.parseColor("#" + Constants.BG_ALPHA + Constants.BG_JOB);
		else if (sectionIndex == Constants.LOVE)
			color = Color.parseColor("#" + Constants.BG_ALPHA + Constants.BG_LOVE);
		else if (sectionIndex == Constants.HEALT)
			color = Color.parseColor("#" + Constants.BG_ALPHA + Constants.BG_HEALT);
		return color;
	}
	
	
	
	private void share(String msg) {
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("*/*");
		shareIntent.putExtra(Intent.EXTRA_TEXT, msg);
		Resources resources = getResources();
		int res = R.drawable.logo_simple;
		Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getResources().getResourcePackageName(res) + '/' + resources.getResourceTypeName(res) + '/' + resources.getResourceEntryName(res) );
		shareIntent.putExtra(Intent.EXTRA_STREAM, uri);		
		startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_message_chooser)));
	}
}
