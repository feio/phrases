package it.psicologiaok.android;

import it.psicologiaok.R;
import it.psicologiaok.android.utils.Constants;
import java.lang.reflect.Field;
import java.util.HashMap;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.ViewConfiguration;

class CommonActivity extends SherlockFragmentActivity {

	private final boolean TEST = false;

	protected SharedPreferences prefs; // Usare sempre questo per la persistenza
	protected ActionBar actionbar;
	protected LocationManager lManager;
	protected LocationListener lListener;
	protected Location location;
	protected Tracker tracker;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/* 
		 * In esecuzioni di test viene abilitato lo StrictMode per il debug delle
		 * operazioni onerose sul thread principale e viene inibito l'invio di dati a GA
		*/
		if (TEST) {
			StrictMode.enableDefaults();
			GoogleAnalytics.getInstance(this).setDryRun(true);
		}

		prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

		// Forza la visualizzazione del menu nella actionbar
		getOverflowMenu();

		actionbar = getSupportActionBar();
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}


	@Override
	public void onStart() {
		super.onStart();
		// Google Analytics
		EasyTracker.getInstance(this).activityStart(this);
		tracker = GoogleAnalytics.getInstance(this).getTracker("UA-45155153-1");
		trackViewOpened();
	}
	

	@Override
	public void onStop() {
		super.onStop();
		// Google Analytics
		EasyTracker.getInstance(this).activityStop(this);
	}



	/**
	 * Forza la visualizzazione del menu nella actionbar
	 */
	private void getOverflowMenu() {

		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Tracciamento aperture dell'Activity per Google Analytics
	 */
	private void trackViewOpened() {
		HashMap<String, String> hitParameters = new HashMap<String, String>();
		String activityHumanReadableName = Constants.GA_ACTIVITY_NAMES.get(getClass().getSimpleName()) != null ? Constants.GA_ACTIVITY_NAMES.get(getClass().getSimpleName()) : Constants.GA_ACTIVITY_NAMES.get("");
		hitParameters.put(Fields.HIT_TYPE, "appview");
		hitParameters.put(Fields.SCREEN_NAME, activityHumanReadableName);
		tracker.send(hitParameters);
	}

}