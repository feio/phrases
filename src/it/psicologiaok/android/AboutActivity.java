package it.psicologiaok.android;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import it.psicologiaok.R;
import it.psicologiaok.android.utils.Constants;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutActivity extends CommonActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		// Link al sito
		TextView site = (TextView) findViewById(R.id.site);
		site.setText(Html.fromHtml("<a href='" + Constants.WEBSITE + "'>" + getString(R.string.visit_website) + "</a>"));
		site.setMovementMethod(LinkMovementMethod.getInstance());
		
		// Link socials
		ImageView facebook = (ImageView) findViewById(R.id.facebook);
		facebook.setOnClickListener(this);
		ImageView twitter = (ImageView) findViewById(R.id.twitter);
		twitter.setOnClickListener(this);
		ImageView gooleplus = (ImageView) findViewById(R.id.google);
		gooleplus.setOnClickListener(this);
		ImageView youtube = (ImageView) findViewById(R.id.youtube);
		youtube.setOnClickListener(this);
		
		// Sviluppatori
		TextView devs = (TextView) findViewById(R.id.developers);
		String text = "";
		String[] devsNames = getResources().getStringArray(R.array.devs_names);
		String[] devsLinks = getResources().getStringArray(R.array.devs_links);				
		for (int i = 0; i < devsNames.length; i++) {
			text += "<a href='" + devsLinks[i] + "'>" + devsNames[i] + "</a><br\\>";			
		}
		devs.append(Html.fromHtml(text));
		devs.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.facebook:
			String facebookScheme;
			Intent facebookIntent;
			try{
	        	facebookScheme = "fb://profile/" + Constants.FACEBOOK_PROFILE_ID;
				facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookScheme));			
	        } catch(Exception e) {
	        	facebookScheme = "http://facebook.com/" + Constants.FACEBOOK_PROFILE;
	        	facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookScheme));
	        }
			startActivity(facebookIntent);
			break;
		case R.id.twitter:
			try {
				intent = new Intent(Intent.ACTION_VIEW,
						Uri.parse("twitter://user?user_id="
								+ Constants.TWITTER_PROFILE));
				startActivity(intent);
			} catch (Exception e) {
				startActivity(new Intent(
						Intent.ACTION_VIEW,
						Uri.parse("http://www.twitter.com/" + Constants.TWITTER_PROFILE)));
			}
			break;
		case R.id.youtube:
			intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("http://www.youtube.com/user/" + Constants.YOUTUBE_PROFILE));
			startActivity(intent);
			break;
		case R.id.google:
			intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("https://plus.google.com/communities/"
					+ Constants.GOOGLEPLUS_PROFILE));
			startActivity(intent);
			break;
		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.save).setVisible(false);
		menu.findItem(R.id.about).setVisible(false);
		menu.findItem(R.id.settings).setVisible(false);
		menu.findItem(R.id.back).setVisible(true);
		return super.onPrepareOptionsMenu(menu);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.back:
				super.onBackPressed();
				break;
		}
		return false;
	}

}
