package it.psicologiaok.android;

import it.psicologiaok.R;
import it.psicologiaok.android.utils.Constants;
import it.psicologiaok.android.utils.TimePickerFragment;
import java.util.Calendar;
import java.util.Date;
import com.actionbarsherlock.view.MenuItem;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends CommonActivity implements OnTimeSetListener {

	private long alarmTime = -1;
	private ImageView time_label_info, repetition_label_info, topic_label_info;
	private TextView timer_label;
	private CheckBox job, healt, love, personal;
	private Button monday, tuesday, wednesday, thursday, friday, saturday, sunday;
	private String personalString = "";
	private boolean[] selectionConfirmed = new boolean[Constants.SELECTION_SIZE];
	private boolean[] weekSelection = new boolean[8];
	private AlarmManager am = null;
	private Context ctx = null;
	private PendingIntent sender = null;
	private int hour, minute;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ctx = this.getApplicationContext();
		
		readPreference();
		initObject();
		addListenerOnChkIos();
		addListenerToButton();
	}


	private void updatePreference() {
		Editor editor = prefs.edit();
		editor.putBoolean(getString(R.string.job_label), selectionConfirmed[Constants.JOB]);
		editor.putBoolean(getString(R.string.love_label), selectionConfirmed[Constants.LOVE]);
		editor.putBoolean(getString(R.string.health_label), selectionConfirmed[Constants.HEALT]);
		editor.putBoolean(getString(R.string.personal_label), selectionConfirmed[Constants.PERSONAL]);

		editor.putString(Constants.myString, personalString);

		editor.putBoolean(getString(R.string.monday_label), weekSelection[Calendar.MONDAY]);
		editor.putBoolean(getString(R.string.tuesday_label), weekSelection[Calendar.TUESDAY]);
		editor.putBoolean(getString(R.string.wednesday_label), weekSelection[Calendar.WEDNESDAY]);
		editor.putBoolean(getString(R.string.thursday_label), weekSelection[Calendar.THURSDAY]);
		editor.putBoolean(getString(R.string.friday_label), weekSelection[Calendar.FRIDAY]);
		editor.putBoolean(getString(R.string.saturday_label), weekSelection[Calendar.SATURDAY]);
		editor.putBoolean(getString(R.string.sunday_label), weekSelection[Calendar.SUNDAY]);

		editor.putInt(getString(R.string.hour_label), hour);
		editor.putInt(getString(R.string.minute_label), minute);

		editor.commit();

	}

	private void readPreference() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		selectionConfirmed[Constants.JOB] = prefs.getBoolean(getString(R.string.job_label), false);
		selectionConfirmed[Constants.LOVE] = prefs.getBoolean(getString(R.string.love_label), false);
		selectionConfirmed[Constants.HEALT] = prefs.getBoolean(getString(R.string.health_label), false);
		selectionConfirmed[Constants.PERSONAL] = prefs.getBoolean(getString(R.string.personal_label),
				false);

		personalString = prefs.getString(Constants.myString, "");

		weekSelection[Calendar.MONDAY] = prefs.getBoolean(getString(R.string.monday_label), false);
		weekSelection[Calendar.TUESDAY] = prefs.getBoolean(getString(R.string.tuesday_label), false);
		weekSelection[Calendar.WEDNESDAY] = prefs.getBoolean(getString(R.string.wednesday_label), false);
		weekSelection[Calendar.THURSDAY] = prefs.getBoolean(getString(R.string.thursday_label), false);
		weekSelection[Calendar.FRIDAY] = prefs.getBoolean(getString(R.string.friday_label), false);
		weekSelection[Calendar.SATURDAY] = prefs.getBoolean(getString(R.string.saturday_label), false);
		weekSelection[Calendar.SUNDAY] = prefs.getBoolean(getString(R.string.sunday_label), false);

		hour = prefs.getInt(getString(R.string.hour_label),
				Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		minute = prefs.getInt(getString(R.string.minute_label), Calendar.getInstance()
				.get(Calendar.MINUTE));

	}

	private void addListenerToButton() {

		time_label_info.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(v.getContext(), v.getContext().getResources().getString(R.string.time_infos),
						Toast.LENGTH_SHORT).show();
			}
		});

		repetition_label_info.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(v.getContext(),
						v.getContext().getResources().getString(R.string.repetition_infos), Toast.LENGTH_SHORT)
						.show();
			}
		});

		topic_label_info.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(v.getContext(), v.getContext().getResources().getString(R.string.topic_infos),
						Toast.LENGTH_LONG).show();
			}
		});

		monday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.MONDAY] = !weekSelection[Calendar.MONDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.MONDAY]);
			}
		});

		tuesday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.TUESDAY] = !weekSelection[Calendar.TUESDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.TUESDAY]);
			}
		});

		wednesday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.WEDNESDAY] = !weekSelection[Calendar.WEDNESDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.WEDNESDAY]);
			}
		});

		thursday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.THURSDAY] = !weekSelection[Calendar.THURSDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.THURSDAY]);
			}
		});

		friday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.FRIDAY] = !weekSelection[Calendar.FRIDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.FRIDAY]);
			}
		});

		saturday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				weekSelection[Calendar.SATURDAY] = !weekSelection[Calendar.SATURDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.SATURDAY]);
			}
		});

		sunday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				weekSelection[Calendar.SUNDAY] = !weekSelection[Calendar.SUNDAY];
				setButtonSelected((Button)v, weekSelection[Calendar.SUNDAY]);
			}
		});

	}

	private void initObject() {
		// Recupero degli oggetti
		job = (CheckBox) findViewById(R.id.jobCB);
		healt = (CheckBox) findViewById(R.id.healtCB);
		love = (CheckBox) findViewById(R.id.loveCB);
		personal = (CheckBox) findViewById(R.id.personalCB);

		timer_label = (TextView) findViewById(R.id.timer_label);

		time_label_info = (ImageView) findViewById(R.id.time_label_info);
		repetition_label_info = (ImageView) findViewById(R.id.repetition_label_info);
		topic_label_info = (ImageView) findViewById(R.id.topic_label_info);

		// week day button
		monday = (Button) findViewById(R.id.mondayButton);
		tuesday = (Button) findViewById(R.id.tuesdayButton);
		wednesday = (Button) findViewById(R.id.wednesdayButton);
		thursday = (Button) findViewById(R.id.thursdayButton);
		friday = (Button) findViewById(R.id.fridayButton);
		saturday = (Button) findViewById(R.id.saturdayButton);
		sunday = (Button) findViewById(R.id.sundayButton);
		
		iniDataFromSettings();
	}

	private void iniDataFromSettings() {
		timer_label.setText(timeToString(hour, minute));
		setAlarmTime(hour, minute);

		job.setChecked(selectionConfirmed[Constants.JOB]);
		healt.setChecked(selectionConfirmed[Constants.HEALT]);
		love.setChecked(selectionConfirmed[Constants.LOVE]);
		personal.setChecked(selectionConfirmed[Constants.PERSONAL]);
		
		setButtonSelected(monday, weekSelection[Calendar.MONDAY]);
		setButtonSelected(tuesday, weekSelection[Calendar.TUESDAY]);
		setButtonSelected(wednesday, weekSelection[Calendar.WEDNESDAY]);
		setButtonSelected(thursday, weekSelection[Calendar.THURSDAY]);
		setButtonSelected(friday, weekSelection[Calendar.FRIDAY]);
		setButtonSelected(saturday, weekSelection[Calendar.SATURDAY]);
		setButtonSelected(sunday, weekSelection[Calendar.SUNDAY]);
	}


	
	private void addListenerOnChkIos() {

		timer_label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showTimePickerDialog(v);
			}
		});

		job.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectionConfirmed[Constants.JOB] = ((CheckBox) v).isChecked();
			}
		});

		healt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectionConfirmed[Constants.HEALT] = ((CheckBox) v).isChecked();
			}
		});

		love.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectionConfirmed[Constants.LOVE] = ((CheckBox) v).isChecked();
			}
		});

		personal.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectionConfirmed[Constants.PERSONAL] = ((CheckBox) v).isChecked();
				if (selectionConfirmed[Constants.PERSONAL]) {
					popupPersonalPhrase();
				} 
			}
		});

		// Long-click per modifica frase personale
		personal.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				popupPersonalPhrase();
				return true; 
			}
		});

	}

	protected void popupPersonalPhrase() {
		Log.d(Constants.TAG, "Inserting personal phrase");
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
		// Inflating layout
		final View layout = inflater.inflate(R.layout.personal_phrase_dialog,
				(ViewGroup) findViewById(R.id.layout_root));
		dialog.setView(layout);
		final EditText editText = (EditText) layout.findViewById(R.id.editText1);
		if (personalString.length() > 0) 
			editText.setText(personalString);
		dialog.setPositiveButton(getResources().getString(R.string.save_label), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				personalString = editText.getText().toString();
			}
		});
		dialog.show();

	}


	public void setAlarmTime(int hour, int minute) {
		Calendar calendar = Calendar.getInstance();
		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.set(Calendar.SECOND, 0);
		currentCalendar.set(Calendar.MILLISECOND, 0);

		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		if (calendar.before(currentCalendar)) {
			alarmTime = 86400000 + calendar.getTimeInMillis();
		} else {
			alarmTime = calendar.getTimeInMillis();
		}
		this.hour = hour;
		this.minute = minute;
	}


	@Override
	protected void onResume() {
		super.onResume();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.about:
				startActivity(new Intent(this, AboutActivity.class));
				break;

			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				break;

			case R.id.save:
				boolean isValid = true;
				boolean almostOneSelection = false;
				String errorMessage = "";

				// Verify if exist a valid alarm timer
				if (alarmTime == -1) {
					errorMessage = ctx.getString(R.string.time_alarm_error) + " ";
					isValid = false;
				}
				for (int i = 0; i < selectionConfirmed.length && !almostOneSelection; i++) {
					almostOneSelection = almostOneSelection | selectionConfirmed[i];
				}
				// verify if exist almost one combo box selected
				if (!almostOneSelection) {
					isValid = false;
					errorMessage += ctx.getString(R.string.selection_error);
				}
				
				if (personal.isChecked() && personalString.length() == 0) {
					isValid = false;
					errorMessage += ctx.getString(R.string.personal_phrase_error);
				}

				if (isValid) {
					updatePreference();

					Intent intent = new Intent(ctx, it.psicologiaok.android.reciever.AlarmReciever.class);
					intent.putExtra(Constants.SELECTION, selectionConfirmed);
					intent.putExtra(Constants.WEEK_SELECTION, weekSelection);

					sender = PendingIntent.getBroadcast(ctx, Constants.ALARM_CODE, intent,
							PendingIntent.FLAG_CANCEL_CURRENT);
					am = (AlarmManager) getSystemService(ALARM_SERVICE);
					am.set(AlarmManager.RTC_WAKEUP, alarmTime, sender);

					Log.d(Constants.TAG, "Next alarm is set on date " + (new Date(alarmTime)).toString());
					Toast.makeText(this, getString(R.string.confirm_saving), Toast.LENGTH_SHORT).show();
					
				} else {
					Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
				}
		}
		return false;
	}


	/**
	 * Visualizza il TimePickerFragment per la selezione dell'orario
	 * 
	 * @param v
	 */
	private void showTimePickerDialog(View v) {
		TimePickerFragment newFragment = new TimePickerFragment();
		newFragment.show(getSupportFragmentManager(), Constants.TAG);
	}


	@Override
	public void onTimeSet(TimePicker view, int hour, int minute) {
		timer_label.setText(timeToString(hour, minute));
		setAlarmTime(hour, minute);
	}


	/**
	 * Conversione da stringa a array contenente ora e minuti
	 * 
	 * @param timeString
	 * @return
	 */
	public int[] stringToTime(String timeString) {
		int hour = Integer.parseInt(timeString.split(Constants.TIME_SEPARATOR)[0]);
		int minute = Integer.parseInt(timeString.split(Constants.TIME_SEPARATOR)[1]);
		int[] time = { hour, minute };
		return time;
	}


	/**
	 * Conversione da array contenente ora e minuti a stringa adatta ad una TextView
	 * 
	 * @param timeString
	 * @return
	 */
	String timeToString(int hour, int minute) {
		String timeString = hour + ":" + (minute < 10 ? "0" + minute : minute);
		return timeString;
	}
	
	
	private void setButtonSelected(Button btn, boolean selected) {
		if (selected) {
			btn.setTextColor(Color.WHITE);
			btn.setBackgroundResource(R.drawable.btn_default_pressed_holo_light);
		} else {
			btn.setTextColor(Color.BLACK);
			btn.setBackgroundResource(R.drawable.btn_default_normal_holo_light);
		}
	}
}
